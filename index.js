Vue.component('message', {
    props: ['message'],
    template: `
        <div class="card mt-1">
            <div class="card-body">
                <h2 class="card-title">{{message.email}}</h2>
                <p class="card-text">{{message.text}}</p>
            </div>
        </div>
    `
});

var vm = new Vue({
    el: '#vm',
    data: {
        email: null,
        text: null,
        messages: []
    },
    methods: {
        addMessage: function() {
            this.messages.push({email: this.email, text: this.text});
            this.email = null;
            this.text = null;
        }
    }
})